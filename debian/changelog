jquery-i18n-properties (1.2.7+dfsg1-5) UNRELEASED; urgency=medium

  * Team upload
  * Declare compliance with policy 4.6.2
  * Add debian/gbp.conf

 -- Yadd <yadd@debian.org>  Fri, 26 May 2023 07:20:24 +0400

jquery-i18n-properties (1.2.7+dfsg1-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.
  * Apply multi-arch hints. + libjs-jquery-i18n-properties: Add Multi-Arch: foreign.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 21 Nov 2022 01:19:33 +0000

jquery-i18n-properties (1.2.7+dfsg1-3) unstable; urgency=medium

  * debian/watch:
    + Fix Github watch URL.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 29 Apr 2021 13:31:25 +0200

jquery-i18n-properties (1.2.7+dfsg1-2) unstable; urgency=medium

  + Source-only upload.

  * debian/copyright:
    + Interpret GPL as GPL-1+ as suggested by Torsten Alteholz.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 03 Feb 2021 22:23:12 +0100

jquery-i18n-properties (1.2.7+dfsg1-1) unstable; urgency=medium

  * Re-upload to unstable as NEW. (Closes: #980416).
    This also solves a missing-dependency-problem for
    openboard(-common). (Closes: #979521).

  * New upstream release.
  * debian/:
    + Repack orig tarball (drop min.js file).
  * debian/*:
    + Update to new upstream origin.
  * debian/patches/2001_use-Debian-s-jquery.patch:
    + Don't use external URL for loading jQuery. Use local paths for loading
      Javascript library files.
  * debian/{control,compat}:
    + Switch to debhelper-compat notation, bump to DH compat level version 13.
  * debian/control:
    + Update Vcs-*: fields. Package has long ago been moved to Salsa.
    + Add myself to Uploaders: field. Drop Daniel Pocock.
    + Add Rules-Requires-Root: field and set it to 'no'.
    + Re-arrange packages in B-D and D fields.
    + Bump Standards-Version: to 4.5.1. No changes needed.
    + White-space fix.
    * Change Section: to 'javascript'.
  * debian/copyright:
    + Rename MIT license tag to Expat.
    + Use https URL in Format: field.
  * debian/rules:
    + Add get-orig-source target.
    + Add dh_clean override, Remove generated *-min.js file during clean-up.
  * debian/{rules,docs,examples}:
    + Handle example files by dh_installexamples.
  * debian/libjs-jquery-i18n-properties.lintian-overrides:
    + Rename lintian tag.
  * debian/upstream/metadata:
    + Add file. Comply with DEP-12.
  * debian/libjs-jquery-i18n-properties.install:
    + Install -min.js and .js file.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 20 Jan 2021 15:12:04 +0100

jquery-i18n-properties (1.1.0-1) unstable; urgency=low

  * Initial packaging (Closes: #750765)

 -- Daniel Pocock <daniel@pocock.pro>  Thu, 12 Jun 2014 13:47:51 +0200
